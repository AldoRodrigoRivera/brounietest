package com.rivera.aldo.brouniearrc.Adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rivera.aldo.brouniearrc.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Aldo on 14/12/2016.
 */

public class CustomListWork extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> name;

    public CustomListWork(Activity context, ArrayList<String> name) {
        super(context, R.layout.list_item_work, name);
        this.context = context;
        this.name = name;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_item_work, null, true);
        String [] obtenido = name.get(position).split("-");
        String nombre = obtenido [1];
        String img = obtenido [2];
        String cell = obtenido [3];

        TextView txtTitle = (TextView) rowView.findViewById(R.id.name);
        TextView txtphone = (TextView) rowView.findViewById(R.id.phone);
        ImageView image = (ImageView) rowView.findViewById(R.id.imgProf);

        txtTitle.setText(nombre);
        txtphone.setText(cell);

        File imgFile = new  File(img);
        if(imgFile.exists()){
            image.setImageDrawable(Drawable.createFromPath(img));
        }


        return rowView;
    }



}
