package com.rivera.aldo.brouniearrc.Adapters;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rivera.aldo.brouniearrc.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Aldo on 14/12/2016.
 */

public class CustomListProject extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> name;
    public CustomListProject(Activity context, ArrayList<String> name) {
        super(context, R.layout.item_grid, name);
        this.context = context;
        this.name = name;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.item_grid, null, true);

        String [] obtenido = name.get(position).split("-");
        String nombre = obtenido [1];
        String imgg = obtenido [2];

        TextView txtTitle = (TextView) rowView.findViewById(R.id.title);
        ImageView img = (ImageView) rowView.findViewById(R.id.img);

        File imgFile = new  File(imgg);
        System.out.println(imgg);
        if(imgFile.exists()){
            img.setImageDrawable(Drawable.createFromPath(imgg));
        }else{
            img.setImageResource(R.drawable.pic);
        }
        txtTitle.setText(nombre);


        return rowView;
    }



}
