package com.rivera.aldo.brouniearrc.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rivera.aldo.brouniearrc.R;

import java.util.ArrayList;

/**
 * Created by Aldo on 14/12/2016.
 */

public class CustomListTeams extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> name;
    public CustomListTeams(Activity context, ArrayList<String> name) {
        super(context, R.layout.list_item, name);
        this.context = context;
        this.name = name;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_item, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.name);
        txtTitle.setText(name.get(position));


        return rowView;
    }



}
