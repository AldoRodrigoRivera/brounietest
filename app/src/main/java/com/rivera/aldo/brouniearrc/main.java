package com.rivera.aldo.brouniearrc;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Aldo on 19/09/2017.
 */

public class main extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/LibreBarcode39Text-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
