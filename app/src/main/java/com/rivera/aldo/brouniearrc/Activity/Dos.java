package com.rivera.aldo.brouniearrc.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.rivera.aldo.brouniearrc.Adapters.CustomListWork;
import com.rivera.aldo.brouniearrc.DataBase.SqliteHelper;
import com.rivera.aldo.brouniearrc.R;

import java.util.ArrayList;

public class Dos extends AppCompatActivity {

    ListView list;
    ArrayList<String> workTeams;
    String namePass;
    FloatingActionButton add;
    CustomListWork adapter;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dos);

        list = (ListView) findViewById(R.id.list);
        add =(FloatingActionButton)findViewById(R.id.btnAdd);

        getSupportActionBar().hide();

        namePass = getIntent().getStringExtra("name");

        teamRecords();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String [] row = workTeams.get(position).split("-");
                Intent i = new Intent(Dos.this,Profile.class);
                i.putExtra("id",row[0]);
                i.putExtra("name",row[1]);
                i.putExtra("image",row[2]);
                i.putExtra("phone",row[3]);
                startActivity(i);
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogo();
            }
        });
    }

    private void teamRecords() {
        SqliteHelper helper = new SqliteHelper(Dos.this, "Brou",null,1);
        workTeams = helper.showWorkers(helper,namePass);
        adapter = new CustomListWork(Dos.this, workTeams);
        list.setAdapter(adapter);
    }

    private void mostrarDialogo() {
        final EditText name = new EditText(this);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Add new teammate")
                .setView(name)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newName = String.valueOf(name.getText().toString());
                        if(newName.equals("")){
                            Toast.makeText(Dos.this, "We need the name, please.", Toast.LENGTH_SHORT).show();
                        }else{
                            SqliteHelper helper = new SqliteHelper(Dos.this, "Brou",null,1);
                            helper.insertW(name.getText().toString(),Dos.this,helper,namePass);
                            teamRecords();  
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }


    public void back(View view) {
        finish();
    }
}
