package com.rivera.aldo.brouniearrc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rivera.aldo.brouniearrc.Adapters.CustomListTeams;
import com.rivera.aldo.brouniearrc.R;

import java.util.ArrayList;

public class Uno extends AppCompatActivity {

    ListView list;
    ArrayList<String> teams;
    CustomListTeams adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uno);
        list = (ListView) findViewById(R.id.list);

        getSupportActionBar().hide();

        teams = new ArrayList<>();
        teams.add("Sells Department");
        teams.add("Develop Department");
        teams.add("Gestion Department");
        teams.add("Desing Department");
        teams.add("Marketing Department");

        adapter = new CustomListTeams(Uno.this, teams);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProgressDialog pdia = ProgressDialog.show(Uno.this,"Wait a second please","Getting the list",false,false);
                Intent i = new Intent(Uno.this,Dos.class);
                i.putExtra("name",teams.get(position));
                pdia.dismiss();
                startActivity(i);
            }
        });
    }

    public void close(View view) {
        finish();
    }
}
