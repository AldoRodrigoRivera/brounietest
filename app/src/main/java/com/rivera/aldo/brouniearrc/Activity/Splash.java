package com.rivera.aldo.brouniearrc.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.rivera.aldo.brouniearrc.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Splash extends AppCompatActivity {

    public final int TIEMPO =3000;                           /*SPLASH*/
    android.os.Handler Handler = new android.os.Handler();   /*SPLASH*/
    Runnable run = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();


        Handler.postDelayed(run = new Runnable() {
            @Override
            public void run() {
                Handler.removeCallbacks(run);
                startActivity(new Intent(Splash.this,Uno.class));
                finish();
            }
        },TIEMPO);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
